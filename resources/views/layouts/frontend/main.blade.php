<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js IE lt-ie9 lt-ie8 lt-ie7"></html>
<![endif]-->
<!--[if IE 7]>
<html class="no-js IE lt-ie9 lt-ie8"></html>
<![endif]-->
<!--[if IE 8]>
<html class="no-js IE lt-ie9"></html>
<![endif]-->
<!--[if gt IE 8]>
<html class="no-js IE gt-ie8"></html>
<![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <!-- title -->
    <title>@yield('title')</title>

    @yield('meta')

    <!-- fav icon -->
    <link href="{{asset('images/favicon.png')}}" rel="shortcut icon">

    <!-- css => style sheet -->
    <link href="{{asset('css/frontstyle.css')}}" media="screen" rel="stylesheet" type="text/css">
    <!-- css => responsive sheet -->
    <link href="{{asset('css/responsive.css')}}" media="screen" rel="stylesheet" type="text/css">


    <!--[if lt IE 9]><!-->
    <!-- css for ie -->
    <link href="{{asset('css/ie.css')}}" media="screen" rel="stylesheet" type="text/css">
    <!--<![endif]-->

</head>

<body id="top" class="page">

<!--[if lt IE 9]>
<p class="browsehappy">
    You are using an
    <strong>outdated</strong>
    browser. Please
    <a href="http://browsehappy.com/">upgrade your browser</a>
    to improve your experience.
</p>
<![endif]-->

<div class="loadingContainer">
    <div class="loading">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div><!-- end of loading -->
</div><!-- end of loading container -->
<div class="allWrapper">

    @yield('content')

</div><!-- end of all wrapper -->

<!-- JQuery => javascript libs -->
<script src="{{asset('vendor/jquery/jquery.js')}}" type="text/javascript"></script>
<!-- online JQuery libs -->
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script> -->

<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>

<!-- JavaScript Files ================================================== -->
<script src="{{asset('js/compiler.js')}}" type="text/javascript"></script>
<script src="{{asset('js/scripts.js')}}" type="text/javascript"></script>

<!-- Proper JS -->
{{--<script src="{{asset('vendor/popper/popper.js')}}" type="text/javascript"> </script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

<!-- BootStrap JavaScript ================================================== -->
<script src="{{asset('vendor/bootstrap/js/bootstrap.js')}}" type="text/javascript"></script>


<!-- Switcher JavaScript ================================================== -->
<script src="{{asset('js/switcher.js')}}" type="text/javascript"></script>

<!-- google maps -->
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

</body>
</html>

