<nav class="mainMenu mainNav" id="mainNav">
    <ul class="navTabs">
        <li><a href="home-1.html" class="active">Home</a></li>
        <li><a href="#">News</a></li>
        <li><a href="#">Schedule</a></li>
        <li><a href="#">Score</a></li>
        <li><a href="#">Video</a></li>
        <li><a href="#">Services</a></li>
        <li><a href="#">Contact</a></li>
        <li class="login formTop">
            <button class="formSwitcher"  data-toggle="modal" data-target="#loginModal">Sing up</button>
            <div class="modal loginModal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="container">
                    <ol class="formWrapper loginFormWrapper" id="loginFormWrapper">
                        <li><h5><i class="fa fa-user"></i>Login Area</h5></li>
                        <li>
                            <form class="loginForm" method="POST">
                                <input class="loginName" id="loginName" name="loginName" placeholder="Name" type="text">
                                <input class="loginPassword" id="loginPassword" name="loginPassword" placeholder="Password" type="password">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">Remember Me</label>
                                <button class="generalBtn loginBtn" type="submit">Login</button>
                            </form>
                        </li>
                        <li class="register"><p><a href="#">Create A new Account</a></p></li>
                    </ol>
                </div><!-- end of container -->
            </div><!-- end of modal -->
            <a href="#">Login</a></li>
    </ul><!-- end of nav tabs -->
</nav><!-- end of main nav -->