<div class="banner-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 sectionTitle">
                <h1 class="sectionHeader">
                    Tittle quisquam est qui dolorem ipsum & Tutorials
                    <span class="generalBorder"></span>
                </h1><!-- end of sectionHeader -->
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
            </div><!-- end of section title -->
        </div><!-- end of row -->

        <div class="row">
            <div class="col-xs-12">
                <div class="banner-img">
                    <img src="{{asset('images/banner/banner-img.jpg')}}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>