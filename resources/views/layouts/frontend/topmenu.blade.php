<div class="topMenu navBar">
    <div class="container">
        <div class="row">

            <ul class="topSocial socialNav col-md-6 col-sm-12">
                <li class="facebook"><a href="#"><i class="animated fa fa-facebook"></i></a></li>
                <li class="twitter"><a href="#"><i class="animated fa fa-twitter"></i></a></li>
                <li class="rss"><a href="#"><i class="animated fa fa-rss"></i></a></li>
                <li class="lang">
                    <form  method="POST" class="langForm">
                        <select name="language" id="language">
                            <option disabled="disabled" value="NoSeclect">Language</option>
                            <option value="En">English</option>
                            <option value="Ar">Arabic</option>
                            <option value="Fr">French</option>
                        </select>
                    </form><!-- end of lang form -->
                </li><!-- end of lang -->
            </ul><!-- end of top social -->

            <div class="topContact col-md-6 col-sm-12">
                <ul>
                    <li class="tele">
                        Call Us Now :
                        <a href="tele:00201065370701">002 0106 5370701</a>
                    </li>
                    <li class="mail">
                        Get In Touch :
                        <a href="mailto:rubelshaikh678@gmail.com">rubelshaikh678@gmail.com</a>
                    </li>
                </ul>
            </div><!-- end of top contacts -->


        </div><!-- end of row -->
    </div><!-- end of container -->
</div>