@extends('layouts.frontend.main')

    @section('meta')
        <!-- meta tags -->
        <meta charset="utf-8">
        <meta content="width=device-width,initial-scale=1,maximum-scale=1" name="viewport">
        <meta content="HTML Tidy for Linux (vers 25 March 2009), see www.w3.org" name="generator">
        <meta content="Mahmoud Bayomy" name="author">
        <meta content="Here is the whole descrption of the website" name="description">

    @endsection

    @section('title', 'NFL | Home')

    @section('content')


            <!-- Page Header -->
            <section class="pageHeader section mainSection scrollAnchor darkSection" id="pageHeader">

                <!-- top menu area -->
                @include('layouts.frontend.topmenu')
                <!-- end of top menu -->

                <!-- Header -->
                <header class="header headerStyle1 style-1" id="header">
                    <div class="sticky scrollHeaderWrapper">
                        <div class="container">
                            <div class="row">

                                <div class="logoWrapper">
                                    <h1 class="logo">
                                        <a class="clearfix" href="{{route('index')}}" title="NFl">
                                            <span class="square">
                                                <span>F</span>
                                            </span>
                                            <span class="text">NFL</span>
                                        </a>
                                    </h1><!-- end of logo -->
                                </div><!-- end of logoWrapper -->

                                <!-- nav-menu area -->
                                @include('layouts.frontend.navmain')
                                <!-- end of nav-menu -->

                                <a href="#" class="generalLink" id="responsiveMainNavToggler"><i class="fa fa-bars"></i></a>
                                <div class="clearfix"></div><!-- end of clearfix -->
                                <div class="responsiveMainNav"></div><!-- end of responsive main nav -->

                            </div><!-- end of row -->
                        </div><!-- end of container -->
                    </div><!-- end of sticky -->
                </header><!-- end of header -->
            </section><!-- end of Page Header -->

            <!-- banner -->
            @include('layouts.frontend.banner')
            <!-- end of banner -->


            <!-- Blog -->
            <section class="blog section mainSection scrollAnchor lightSection" id="blog">
                <div class="sectionWrapper">
                    <div class="container singlePostPage">

                        <div class="row">

                            <div class="col-md-12 sectionTitle">
                                <h2 class="sectionHeader">
                                    Tittle quisquam est qui dolorem ipsum & Tutorials
                                    <span class="generalBorder"></span>
                                </h2><!-- end of sectionHeader -->
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
                            </div><!-- end of section title -->
                        </div><!-- end of row -->

                        <div class="row">

                            <div class="col-md-8">

                                <div class="row">

                                    <article class="col-md-12 post postWide singlePost">
                                        <div class="postWrapper">
                                            <div class="postMedia postSlider carousel2">

                                                <img alt="post sample" src="{{asset('images/blog/1.jpg')}}" title="post sample">


                                                <img alt="post sample" src="{{asset('images/blog/2.jpg')}}" title="post sample">


                                                <img alt="post sample" src="{{asset('images/blog/3.jpg')}}" title="post sample">

                                            </div>
                                            <div class="postContents">
                                                <a href="#" class="postIcon">
                                                    <i class="animated fa fa-newspaper-o"></i>
                                                </a>
                                                <h4 class="postTitle">Blog Tiltle Shall Be Here !</h4>
                                                <p class="postDetails">Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.

                                                    <br />
                                                    <br />

                                                    Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </p>

                                                <ul class="postMeta clearfix">
                                                    <li class="postDate">
                                                        <div class="metaContent">
                                                            <i class="animated fa fa-clock-o"></i>
                                                            Date : Feb 15, 2014
                                                        </div>
                                                    </li>
                                                    <li class="postAuthor">
                                                        <div class="metaContent">
                                                            <i class="animated fa fa-user"></i>
                                                            Author :
                                                            <a href="#" title="author name">Begha</a>
                                                        </div>
                                                    </li>
                                                    <li class="postComments">
                                                        <div class="metaContent">
                                                            <i class="animated fa fa-comment-o"></i>
                                                            <a class="scrollTo" data-scroll="comments" href="#comments">Comments : 11</a>
                                                        </div>
                                                    </li>
                                                </ul>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </article><!-- end of post -->

                                    <div class="clearfix"></div>

                                    <div class="col-md-12 share">

                                        <div class="shareBtns clearfix">
                                            <h5 class="shareHeader title">Share This Article If You Liked It :)</h5><!-- end of share header -->
                                            <div class="shareBtnsWrapper">
                                                <ul class="rrssb-buttons colorful clearfix">
                                                    <li class="facebook">
                                                        <a href="#" class="popup"><i class="fa fa-facebook"></i></a>
                                                    </li>
                                                    <li class="twitter">
                                                        <a href="#" class="popup"><i class="fa fa-twitter"></i></a>
                                                    </li>
                                                    <li class="googleplus">
                                                        <a href="#" class="popup"><i class="fa fa-google-plus"></i></a>
                                                    </li>
                                                    <li class="email">
                                                        <!-- Replace subject with your message using URL Endocding: http://meyerweb.com/eric/tools/dencoder/ -->
                                                        <a href="#"><i class="fa fa-share-alt"></i></a>
                                                    </li>
                                                    <li class="linkedIn">
                                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                                    </li>
                                                    <li class="pinterest">
                                                        <a href="#"><i class="fa fa-pinterest"></i></a>
                                                    </li>
                                                </ul>
                                            </div><!-- end of share btns wrapper -->
                                        </div><!-- end of share btns -->

                                    </div><!-- end of share -->
                                </div><!-- end of row -->


                                <!-- start -->
                                <!-- Welcome -->
                                <section class="welcome section mainSection scrollAnchor lightSection" id="welcome">
                                    <div class="sectionWrapper">

                                        <div class="row">

                                            <div class="col-md-12 sectionTitle">
                                                <h2 class="sectionHeader">
                                                    Welcome To Seven Host , Awesome Hosting Template
                                                    <span class="generalBorder"></span>
                                                </h2><!-- end of sectionHeader -->
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam, adipiscing condimentum tristique vel, eleifend sed turpis. Pellentesque cursus arcu id magna euismod in elementum purus molestie.</p>
                                            </div><!-- end of section title -->

                                        </div><!-- end of row -->

                                        <div class="row">
                                            <div class="servicesCarousel carousel owl-carousel servicesGallary">

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">System Admin</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-1"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Backups &amp; Storage</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-2"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">24 / 7 Live Support</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-3"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Worldwide locations</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-4"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">System Admin</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-1"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Backups &amp; Storage</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-2"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">24 / 7 Live Support</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-3"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Worldwide locations</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-4"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">System Admin</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-1"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Backups &amp; Storage</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-2"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">24 / 7 Live Support</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-3"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div><!-- end of single service -->
                                                </div><!-- end of single service wrapper -->

                                                <div class="col-md-3 col-sm-6 item service singleServiceWrapper">
                                                    <div class="singleService">
                                                        <h3 class="serviceName">Worldwide locations</h3>
                                                        <div class="serviceIcon">
                                                            <div class="servicesIconBase servicesBg-4"></div>
                                                        </div>
                                                        <p class="servicesDescription">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lorem quam.</p>
                                                        <a class="readMore generalLink" href="#">Read More</a>
                                                    </div>
                                                    <!-- end of single service -->
                                                </div><!-- end of single service wrapper -->
                                            </div><!-- end of services gallary -->

                                        </div><!-- end of row -->
                                    </div><!-- end of section wrapper -->
                                </section><!-- end welcome section -->
                                <!-- start -->




                            </div><!-- end of col-md-8 -->

                            <aside class="col-md-4 sidebar">

                                <div class="widget searchWidget">
                                    <div class="widgetBody">
                                        <form method="GET" class="sideSearch">
                                            <ul class="clearfix">
                                                <li><input type="search" value="search here" onblur="if(this.value=='')this.value='search here'" onfocus="if(this.value=='search here')this.value=''" name="s" id="sideSearch" class="sideSearch"></li>
                                                <li><button type="submit"><i class="animated fa fa-search"></i></button></li>
                                            </ul>
                                        </form><!-- end of side search -->
                                    </div><!-- end of widget body -->
                                </div><!-- end of widget --><!-- end of search widget -->

                                <div class="widget categoriesWidget">
                                    <h5 class="widgetHeader">Categories</h5><!-- end of widget header -->
                                    <div class="widgetBody">
                                        <ul class="list">
                                            <li><a href="#" title="Self Development Courses">Self Development Courses</a></li>
                                            <li><a href="#" title="Adminsitrative Courses">Adminsitrative Courses</a></li>
                                            <li><a href="#" title="Social Courses">Social Courses</a></li>
                                            <li><a href="#" title="Computer & It Courses">Computer & It Courses</a></li>
                                            <li><a href="#" title="Language Courses">Language Courses</a></li>
                                            <li><a href="#" title="Health & Medical">Health & Medical</a></li>
                                            <li><a href="#" title="Policy & Law Courses">Policy & Law Courses</a></li>
                                            <li><a href="#" title="Other Courses">Other Courses</a></li>
                                        </ul>
                                    </div><!-- end of widget body -->
                                </div><!-- end of widget --><!-- end of categories Widget -->

                                <div class="widget">
                                    <h5 class="widgetHeader clearfix">
                                        Latest Topics
                                        <span class="tickerControl">
                                            <i class="animated fa fa-angle-left" id="ticker-prev"></i>
                                            <i class="animated fa fa-angle-right" id="ticker-next"></i>
                                        </span>
                                    </h5><!-- end of widget header -->

                                    <div class="widgetBody">

                                        <ul id="ticker" class="ticker">

                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>


                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>


                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>


                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>


                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>


                                            <li class="clearfix">
                                                <article class="post">
                                                    <div class="postContents">
                                                        <h5 class="postTitle"><a href="#">Blog Tiltle Shall Be Here !</a></h5>
                                                        <ul class="postMeta">
                                                            <li class="postDate">Feb 16, 2014</li>
                                                        </ul>
                                                    </div><!-- end of post  contents -->
                                                </article><!-- end of post -->
                                            </li>

                                        </ul><!-- end of ticker -->
                                    </div><!-- end of widget body -->
                                </div><!-- end of widget -->

                                <div class="widget tweets">
                                    <h5 class="widgetHeader">Latest from Twitter</h5><!-- end of widget header -->
                                    <div class="widgetBody carousel3">

                                        <div class="tweet">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation quat.</p>

                                            <span class="tweetTime">Afew minutes ago</span>

                                        </div><!-- end of tweet -->


                                        <div class="tweet">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation quat.</p>

                                            <span class="tweetTime">Afew minutes ago</span>

                                        </div><!-- end of tweet -->


                                        <div class="tweet">
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation quat.</p>

                                            <span class="tweetTime">Afew minutes ago</span>

                                        </div><!-- end of tweet -->


                                    </div><!-- end of widget body -->
                                    <img src="{{asset('images/tweet.png')}}" alt="tweet">

                                    <div class="clearfix"></div>
                                </div><!-- end of widget -->

                            </aside><!-- end of sidebar -->
                        </div>



                        <div class="clearfix"></div>

                    </div><!-- end of container -->
                </div><!-- end of section wrapper -->
            </section><!-- end blog section -->


            @include('layouts.frontend.footer')


    @endsection