<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> NFL | Registration </title>

    <link rel="stylesheet" href={{asset("vendor/bootstrap/css/bootstrap.min.css")}}>
    <link rel="stylesheet" href={{asset('css/auth.css')}}>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="container">
    <div class="container-fluid">
        <form action="{{route('register')}}" method="POST" class="register-form">
            {{csrf_field()}}
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4">

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="firstName">NAME</label>
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                        <label for="email">EMAIL</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">PASSWORD</label>
                        <input id="password" type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4 col-lg-4">
                    <label for="password-confirm">CONFIRM PASSWORD</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                    <a href="{{route('login')}}"><button class="btn btn-default regbutton">Login</button></a>

                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
                    <button class="btn btn-default logbutton">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script src={{asset('vendor/jquery/jquery.min.js')}}></script>
<script src={{asset('vendor/bootstrap/js/bootstrap.min.js')}}></script>
<script src={{asset('js/app.js')}}></script>
</body>
</html>
